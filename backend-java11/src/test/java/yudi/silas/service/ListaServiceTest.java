package yudi.silas.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import yudi.silas.entity.Lista;
import yudi.silas.exception.ListaExisteException;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ListaServiceTest {

    @Autowired
    private ListaService listaService;

    @AfterEach
    public void tearDown() {
        Iterator<Lista> iterator = listaService.getAll().iterator();

        while (iterator.hasNext()) {
            listaService.deleteList(iterator.next().getId());
        }
    }

    @Test
    public void testAdicionaLista() {
        saveLista("testAdicionaLista");
    }

    @Test
    public void testAdicionaDuasListas() {
        saveLista("testAdicionaLista1");
        saveLista("testAdicionaLista2");
    }

    @Test
    public void testAdicionaListaJaExistente() {
        saveLista("testAdicionaListaJaExistente");

        assertThrows(
                ListaExisteException.class,
                () -> saveLista("testAdicionaListaJaExistente")
        );
    }

    @Test
    public void testUpdateLista() {
        Lista lista = saveLista("testUpdateLista");
        lista.setNome("Teste Outro Nome");
        lista = listaService.saveList(lista);
        assertListaSalva(lista, "Teste Outro Nome");
    }

    @Test
    public void testDeleteLista() {
        Lista lista = saveLista("testDeleteLista");
        listaService.deleteList(lista.getId());
        assertListaNaoSalva(lista);
    }

    @Test
    public void testGetAll() {
        assertEquals(0, getAll().size());

        Lista lista = saveLista("testGetAll");
        assertEquals(1, getAll().size());

        lista.setNome("testGetAllUpdate");
        listaService.saveList(lista);
        assertEquals(1, getAll().size());

        saveLista("nova lista");
        assertEquals(2, getAll().size());
    }

    private Lista saveLista(String nome) {
        Lista lista = new Lista();
        lista.setNome(nome);
        lista = listaService.saveList(lista);

        assertListaSalva(lista, nome);

        return lista;
    }

    private void assertListaSalva(Lista lista, String nome) {
        Optional<Lista> listaOptional = listaService.getByName(nome);

        assertTrue(listaOptional.isPresent());
        assertTrue(listaOptional.get().equals(lista));
        assertNotNull(lista.getId());
        assertEquals(nome, lista.getNome());
    }

    private void assertListaNaoSalva(Lista lista) {
        Optional<Lista> listaOptional = listaService.getByName(lista.getNome());
        assertFalse(listaOptional.isPresent());
    }

    private List<Lista> getAll() {
        return (List<Lista>) listaService.getAll();
    }
}
