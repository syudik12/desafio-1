package yudi.silas.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import yudi.silas.entity.Lista;
import yudi.silas.entity.Tarefa;
import yudi.silas.exception.ListaNaoExisteException;
import yudi.silas.exception.TarefaExisteException;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TarefaServiceTest {

    @Autowired
    private ListaService listaService;

    @Autowired
    private TarefaService tarefaService;

    private Lista lista;

    @BeforeEach
    public void setup() {
        lista = new Lista();
        lista.setNome("TarefaServiceTest");
        listaService.saveList(lista);
    }

    @AfterEach
    public void tearDown() {
        Iterator<Lista> iterator = listaService.getAll().iterator();

        while (iterator.hasNext()) {
            listaService.deleteList(iterator.next().getId());
        }
    }

    @Test
    public void testAdicionaTarefa() {
        saveTarefa("testAdicionaTarefa");
    }

    @Test
    public void testAdicionaDuasTarefasNaMesmaLista() {
        saveTarefa("testAdicionaTarefa1");
        saveTarefa("testAdicionaTarefa2");
    }

    @Test
    public void testAdicionaMesmaTarefaEmListasDiferentes() {
        Lista lista1 = new Lista();
        lista1.setNome("lista1");

        Lista lista2 = new Lista();
        lista2.setNome("lista2");

        listaService.saveList(lista1);
        listaService.saveList(lista2);

        saveTarefa("testAdicionaTarefa", lista1.getId());
        saveTarefa("testAdicionaTarefa", lista2.getId());
    }

    @Test
    public void testAdicionaTarefaEmListaInexistente() {
        Integer idLista = lista.getId();
        listaService.deleteList(idLista);

        assertThrows(
                ListaNaoExisteException.class,
                () -> saveTarefa("testAdicionaTarefaEmListaInexistente", idLista)
        );
    }

    @Test
    public void testAdicionaTarefaJaExistenteNaLista() {
        saveTarefa("testAdicionaTarefaJaExistenteNaLista");

        assertThrows(
                TarefaExisteException.class,
                () -> saveTarefa("testAdicionaTarefaJaExistenteNaLista")
        );
    }

    @Test
    public void testDelete() {
        Tarefa tarefa = saveTarefa("testDelete");

        tarefaService.delete(tarefa.getId());
        assertTarefaNaoSalva(tarefa);
    }

    @Test
    public void testGetAllAndDeleteAll() {
        saveTarefa("testDeleteAll1");
        saveTarefa("testDeleteAll2");
        saveTarefa("testDeleteAll3");
        List<Tarefa> tarefas = getAllTarefas();
        assertEquals(3, tarefas.size());

        tarefaService.deleteAll(lista.getId());
        tarefas = getAllTarefas();
        assertEquals(0, tarefas.size());
    }

    @Test
    public void testGetById() {
        Tarefa tarefa = saveTarefa("testGetById");
        Integer id = tarefa.getId();
        Optional<Tarefa> tarefaOptional = tarefaService.getById(id);

        assertTrue(tarefaOptional.isPresent());
        assertEquals(tarefa, tarefaOptional.get());
    }

    private List<Tarefa> getAllTarefas() {
        return (List<Tarefa>) tarefaService.getAllByLista(lista.getId());
    }

    private Tarefa saveTarefa(String nome) {
        return saveTarefa(nome, lista.getId());
    }

    private Tarefa saveTarefa(String nome, Integer idLista) {
        Tarefa tarefa = new Tarefa();
        tarefa.setNome(nome);
        tarefa.setLista(idLista);
        tarefa = tarefaService.saveTarefa(tarefa);

        assertTarefaSalva(tarefa, nome);

        return tarefa;
    }

    private void assertTarefaSalva(Tarefa tarefa, String nome) {
        Optional<Tarefa> tarefaOptional = tarefaService.getByNomeAndLista(nome, tarefa.getLista());

        assertTrue(tarefaOptional.isPresent());
        assertTrue(tarefaOptional.get().equals(tarefa));
        assertNotNull(tarefa.getId());
        assertEquals(nome, tarefa.getNome());
    }

    private void assertTarefaNaoSalva(Tarefa tarefa) {
        Optional<Tarefa> tarefaOptional = tarefaService.getByNomeAndLista(tarefa.getNome(), tarefa.getLista());
        assertFalse(tarefaOptional.isPresent());
    }
}
