package yudi.silas.exception;

public class ListaNaoExisteException extends RuntimeException {

    private static final String MESSAGE = "Esta lista não existe.";

    public ListaNaoExisteException() {
        super(MESSAGE);
    }

}
