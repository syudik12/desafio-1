package yudi.silas.exception;

public class ListaExisteException extends RuntimeException {

    private static final String MESSAGE = "Esta lista já existe. Por favor, escolha outro nome.";

    public ListaExisteException() {
        super(MESSAGE);
    }

}
