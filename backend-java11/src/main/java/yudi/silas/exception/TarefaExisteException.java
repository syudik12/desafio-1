package yudi.silas.exception;

public class TarefaExisteException extends RuntimeException {

    private static final String MESSAGE = "Esta tarefa já existe nesta lista. Por favor, escolha outro nome.";

    public TarefaExisteException() {
        super(MESSAGE);
    }

}
