package yudi.silas.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import yudi.silas.entity.Lista;

import java.util.Optional;

@Repository
public interface ListaRepository extends CrudRepository<Lista, Integer> {

    Optional<Lista> findByNome(String nome);

}
