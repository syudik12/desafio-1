package yudi.silas.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import yudi.silas.entity.Tarefa;

import java.util.Optional;

@Repository
public interface TarefaRepository extends CrudRepository<Tarefa, Integer> {

    Iterable<Tarefa> findAllByLista(Integer lista);

    Optional<Tarefa> findByNomeAndLista(String nome, Integer lista);
}
