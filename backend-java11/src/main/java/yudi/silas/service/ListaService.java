package yudi.silas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yudi.silas.entity.Lista;
import yudi.silas.exception.ListaExisteException;
import yudi.silas.repository.ListaRepository;

import java.util.Optional;

@Service
public class ListaService {

    @Autowired
    private ListaRepository listaRepository;

    @Autowired
    private TarefaService tarefaService;

    public Optional<Lista> getById(Integer lista) {
        return listaRepository.findById(lista);
    }

    public Optional<Lista> getByName(String nome) {
        return listaRepository.findByNome(nome);
    }

    public Iterable<Lista> getAll() {
        return listaRepository.findAll();
    }

    public Lista saveList(Lista lista) {
        Optional<Lista> listaExistente = getByName(lista.getNome());

        if (listaExistente.isPresent() && listaExistente.get().getId() != lista.getId()) {
            throw new ListaExisteException();
        }

        return listaRepository.save(lista);
    }

    public void deleteList(Integer lista) {
        tarefaService.deleteAll(lista);

        Optional<Lista> listaOptional = getById(lista);

        if (listaOptional.isPresent()) {
            listaRepository.deleteById(lista);
        }
    }
}
