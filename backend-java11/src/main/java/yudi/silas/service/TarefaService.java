package yudi.silas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yudi.silas.entity.Tarefa;
import yudi.silas.exception.ListaNaoExisteException;
import yudi.silas.exception.TarefaExisteException;
import yudi.silas.repository.TarefaRepository;

import javax.swing.text.html.Option;
import java.util.Optional;

@Service
public class TarefaService {

    @Autowired
    private TarefaRepository tarefaRepository;
    @Autowired
    private ListaService listaService;

    public Optional<Tarefa> getById(Integer tarefa) {
        return tarefaRepository.findById(tarefa);
    }

    public Iterable<Tarefa> getAllByLista(Integer lista) {
        return tarefaRepository.findAllByLista(lista);
    }

    public Optional<Tarefa> getByNomeAndLista(String nome, Integer lista) {
        return tarefaRepository.findByNomeAndLista(nome, lista);
    }

    public Tarefa saveTarefa(Tarefa tarefa) {
        int lista = tarefa.getLista();

        if (!listaService.getById(lista).isPresent()) {
            throw new ListaNaoExisteException();
        }

        Optional<Tarefa> old = getByNomeAndLista(tarefa.getNome(), lista);
        if (old.isPresent() && old.get().getId() != tarefa.getId()) {
            throw new TarefaExisteException();
        }

        return tarefaRepository.save(tarefa);
    }

    public void delete(Integer tarefa) {
        if (getById(tarefa).isPresent()) {
            tarefaRepository.deleteById(tarefa);
        }
    }

    public void deleteAll(Integer lista) {
        Iterable<Tarefa> deletables = tarefaRepository.findAllByLista(lista);
        tarefaRepository.deleteAll(deletables);
    }
}
