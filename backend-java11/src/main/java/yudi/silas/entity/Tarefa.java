package yudi.silas.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity(name = "Tarefa")
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"nome", "lista"})})
public class Tarefa {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotBlank(message = "O nome da tarefa é obrigatório.")
    private String nome;

    private String descricao;

    private boolean finalizada;

    @NotNull(message = "A tarefa deve pertencer a uma lista.")
    private Integer lista;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isFinalizada() {
        return finalizada;
    }

    public void setFinalizada(boolean finalizada) {
        this.finalizada = finalizada;
    }

    public Integer getLista() {
        return lista;
    }

    public void setLista(Integer lista) {
        this.lista = lista;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tarefa tarefa = (Tarefa) o;
        return finalizada == tarefa.finalizada &&
                nome.equals(tarefa.nome) &&
                Objects.equals(descricao, tarefa.descricao) &&
                lista.equals(tarefa.lista);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nome, descricao, finalizada, lista);
    }
}
