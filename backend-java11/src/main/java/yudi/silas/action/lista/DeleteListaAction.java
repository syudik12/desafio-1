package yudi.silas.action.lista;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import yudi.silas.service.ListaService;

import java.util.HashMap;
import java.util.Map;

@RestController
public class DeleteListaAction {

    private Map<String, Object> response = new HashMap<>();

    @Autowired
    private ListaService listaTarefaService;

    @DeleteMapping("listas/{id}")
    public ResponseEntity<Map<String, Object>> action(@PathVariable Integer id) {
        listaTarefaService.deleteList(id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
