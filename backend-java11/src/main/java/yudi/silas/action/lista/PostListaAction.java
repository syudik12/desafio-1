package yudi.silas.action.lista;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import yudi.silas.entity.Lista;
import yudi.silas.service.ListaService;

import javax.validation.Valid;

@RestController
public class PostListaAction {

    @Autowired
    private ListaService listaTarefaService;

    @PostMapping(value = "listas", consumes = "application/json")
    public ResponseEntity<Lista> action(@RequestBody @Valid Lista lista) {
        lista = listaTarefaService.saveList(lista);
        return new ResponseEntity<>(lista, HttpStatus.CREATED);
    }
}
