package yudi.silas.action.lista;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import yudi.silas.entity.Lista;
import yudi.silas.service.ListaService;

import java.util.Optional;

@RestController
public class GetListaAction {

    @Autowired
    private ListaService listaService;

    @GetMapping("listas/{id}")
    public ResponseEntity<Lista> action(@PathVariable("id") Integer id) {
        Optional<Lista> listaOptional = listaService.getById(id);

        return listaOptional.isPresent()
                ? new ResponseEntity<>(listaOptional.get(), HttpStatus.OK)
                : new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
}
