package yudi.silas.action.lista;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import yudi.silas.entity.Lista;
import yudi.silas.service.ListaService;

@RestController
public class GetAllListaAction {

    @Autowired
    private ListaService listaService;

    @GetMapping("listas")
    public ResponseEntity<Iterable<Lista>> action() {
        Iterable<Lista> listas = listaService.getAll();
        return new ResponseEntity<>(listas, HttpStatus.OK);
    }
}
