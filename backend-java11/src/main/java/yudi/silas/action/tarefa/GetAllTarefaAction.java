package yudi.silas.action.tarefa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import yudi.silas.entity.Tarefa;
import yudi.silas.service.TarefaService;

@RestController
public class GetAllTarefaAction {

    @Autowired
    private TarefaService tarefaService;

    @GetMapping("tarefas/lista/{id}")
    public ResponseEntity<Iterable<Tarefa>> action(@PathVariable Integer id) {
        Iterable<Tarefa> tarefas = tarefaService.getAllByLista(id);
        return new ResponseEntity<>(tarefas, HttpStatus.OK);
    }
}
