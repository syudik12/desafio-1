package yudi.silas.action.tarefa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import yudi.silas.entity.Tarefa;
import yudi.silas.service.TarefaService;

import javax.validation.Valid;

@RestController
public class PutTarefaAction {

    @Autowired
    private TarefaService tarefaService;

    @PutMapping(value = "tarefas", consumes = "application/json")
    public ResponseEntity<Tarefa> action(@RequestBody @Valid Tarefa tarefa) {
        tarefa = tarefaService.saveTarefa(tarefa);
        return new ResponseEntity<>(tarefa, HttpStatus.CREATED);
    }
}
