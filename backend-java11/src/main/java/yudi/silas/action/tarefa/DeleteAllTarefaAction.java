package yudi.silas.action.tarefa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import yudi.silas.service.TarefaService;

import java.util.HashMap;
import java.util.Map;

@RestController
public class DeleteAllTarefaAction {

    private Map<String, Object> response = new HashMap<>();
    private HttpStatus status = HttpStatus.OK;

    @Autowired
    private TarefaService tarefaService;

    @DeleteMapping("tarefas/lista/{id}")
    public ResponseEntity<Map<String, Object>> action(@PathVariable Integer id) {
        tarefaService.deleteAll(id);

        return new ResponseEntity<>(response, status);
    }
}
