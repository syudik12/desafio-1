import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Tarefa} from './tarefa.model';

@Injectable({
  providedIn: 'root',
})
export class TarefaService {
  baseUrl = '/api/tarefas';

  constructor(private http: HttpClient) {
  }

  read(lista: number): Observable<Tarefa[]> {
    const url = `${this.baseUrl}/lista/${lista}`;
    return this.http.get<Tarefa>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  create(tarefa: Tarefa): Observable<Tarefa> {
    return this.http.post<Tarefa>(this.baseUrl, tarefa)
      .pipe(
        map((obj) => obj),
        catchError((e) => this.errorHandler(e))
      );
  }

  delete(tarefa: Tarefa): Observable<Tarefa> {
    const url = `${this.baseUrl}/${tarefa.id}`;
    return this.http.delete<Tarefa>(url)
      .pipe(
        map((obj) => obj),
        catchError((e) => this.errorHandler(e))
      );
  }

  update(tarefa: Tarefa): Observable<Tarefa> {
    return this.http.put<Tarefa>(this.baseUrl, tarefa)
      .pipe(
        map((obj) => obj),
        catchError((e) => this.errorHandler(e))
      );
  }

  reset(lista: number): Observable<any> {
    const url = `${this.baseUrl}/lista/${lista}`;
    return this.http.delete(url)
      .pipe(
        map((obj) => obj),
        catchError((e) => this.errorHandler(e))
      );
  }

  errorHandler(e: any): Observable<any> {
    const message = e?.error?.exception ?? 'Ocorreu um erro inexperado.';
    alert(message);
    return EMPTY;
  }
}
