import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Lista} from './lista.model';

@Injectable({
  providedIn: 'root',
})
export class ListaService {
  baseUrl = '/api/listas';

  constructor(private http: HttpClient) {
  }

  create(lista: Lista): Observable<Lista> {
    return this.http.post<Lista>(this.baseUrl, lista).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  readById(lista: number): Observable<Lista> {
    const url = `${this.baseUrl}/${lista}`;
    return this.http.get<Lista>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  read(): Observable<Lista[]> {
    return this.http.get<Lista[]>(this.baseUrl).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  update(lista: Lista): Observable<Lista> {
    return this.http.put<Lista>(this.baseUrl, lista).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  delete(id: number): Observable<Lista> {
    const url = `${this.baseUrl}/${id}`;
    return this.http.delete<Lista>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  errorHandler(e: any): Observable<any> {
    const message = e?.error?.exception ?? 'Ocorreu um erro inexperado.';
    alert(message);
    return EMPTY;
  }
}
