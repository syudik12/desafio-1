import {Component, Inject, OnInit} from '@angular/core';
import {Lista} from '../../lista.model';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-lista-form',
  templateUrl: './lista-form.component.html',
  styleUrls: ['./lista-form.component.scss']
})
export class ListaFormComponent implements OnInit {

  default: Lista = {
    id: null,
    nome: '',
  };
  lista = this.default;

  constructor(@Inject(MAT_DIALOG_DATA) public data) {
    if (data != null) {
      this.lista = {...data.lista};
    } else {
      this.lista = this.default;
    }
  }

  ngOnInit(): void {
  }
}
