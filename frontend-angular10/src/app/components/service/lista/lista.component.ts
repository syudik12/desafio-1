import {Component, OnInit} from '@angular/core';
import {ListaService} from '../lista.service';
import {Lista} from '../lista.model';
import {MatDialog} from '@angular/material/dialog';
import {ListaFormComponent} from './lista-form/lista-form.component';
import {Router} from "@angular/router";

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {

  listas: Lista[];

  constructor(
    private listaService: ListaService,
    private formLista: MatDialog,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.listaService.read().subscribe(listas => {
      this.listas = listas;
    });
  }

  adicionarLista(): void {
    this.formLista
      .open(ListaFormComponent)
      .afterClosed()
      .subscribe(lista => {
        this.salvaLista(lista);
      });
  }

  salvaLista(lista: Lista): void {
    this.listaService.create(lista).subscribe(result => {
      this.listas.push(result);
    });
  }

  editaLista(lista: Lista): void {
    this.formLista.open(ListaFormComponent,
      {width: 'auto', data: {lista: lista}}
    )
      .afterClosed()
      .subscribe(result => {
        if (result != null) {
          this.updateLista(lista, result);
        }
      });
  }

  updateLista(listaOld: Lista, listaNew: Lista): void {
    this.listaService.update(listaNew).subscribe(result => {
      const index = this.listas.indexOf(listaOld);
      this.listas[index] = result;
      this.router.navigate(['']);
    });
  }
}
