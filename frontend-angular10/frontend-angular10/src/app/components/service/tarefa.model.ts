export interface Tarefa {
  id?: number;
  nome: string;
  descricao?: string;
  finalizada: boolean;
  lista: number;
}
