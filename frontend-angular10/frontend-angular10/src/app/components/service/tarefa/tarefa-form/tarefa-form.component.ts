import {Component, Inject, OnInit} from '@angular/core';
import {Tarefa} from '../../tarefa.model';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-tarefa-form',
  templateUrl: './tarefa-form.component.html',
  styleUrls: ['./tarefa-form.component.scss']
})
export class TarefaFormComponent implements OnInit {

  default: Tarefa = {
    id: null,
    nome: '',
    descricao: '',
    finalizada: false,
    lista: 0
  };
  tarefa = this.default;

  constructor(@Inject(MAT_DIALOG_DATA) public data) {
    if (data != null) {
      this.tarefa = {...data.tarefa};
    } else {
      this.tarefa = this.default;
    }
  }

  ngOnInit(): void {
  }
}
