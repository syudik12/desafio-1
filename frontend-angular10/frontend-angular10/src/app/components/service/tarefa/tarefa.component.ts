import {Component, OnInit} from '@angular/core';
import {ListaService} from '../lista.service';
import {TarefaService} from '../tarefa.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Tarefa} from '../tarefa.model';
import {Lista} from '../lista.model';
import {MatDialog} from '@angular/material/dialog';
import {TarefaFormComponent} from './tarefa-form/tarefa-form.component';
import {SimpleDialogComponent} from '../../template/simple-dialog/simple-dialog.component';

@Component({
  selector: 'app-tarefa',
  templateUrl: './tarefa.component.html',
  styleUrls: ['./tarefa.component.scss']
})
export class TarefaComponent implements OnInit {

  tarefas: Tarefa[];
  lista: Lista;

  constructor(
    private listaService: ListaService,
    private tarefaService: TarefaService,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      const idLista = params['id'];
      this.getLista(idLista);
      this.populaLista(idLista);
    });
  }

  getLista(idLista: number): void {
    this.listaService.readById(idLista).subscribe(lista => {
      this.lista = lista;
    });
  }

  populaLista(idLista: number): void {
    this.tarefaService.read(idLista).subscribe(tarefas => {
      this.tarefas = tarefas;
    });
  }

  toggleTarefaFinalizada(tarefa: Tarefa): void {
    tarefa.finalizada = !tarefa.finalizada;
    this.updateTarefa(tarefa, tarefa);
  }

  editarTarefa(tarefa: Tarefa): void {
    this.dialog.open(TarefaFormComponent,
      {width: 'auto', data: {tarefa: tarefa}}
    )
      .afterClosed()
      .subscribe(result => {
        if (result != null) {
          result.lista = this.lista.id;
          this.updateTarefa(tarefa, result);
        }
      });
  }

  salvarTarefa(tarefa: Tarefa): void {
    this.tarefaService.create(tarefa).subscribe(result => {
      this.tarefas.push(result);
    });
  }

  updateTarefa(tarefaOld: Tarefa, tarefaNew: Tarefa): void {
    this.tarefaService.update(tarefaNew).subscribe(result => {
      const index = this.tarefas.indexOf(tarefaOld);
      this.tarefas[index] = result;
    });
  }

  deleteTarefa(tarefa: Tarefa): void {
    this.confirmaRemocao(tarefa);
  }

  confirmaRemocao(tarefa: Tarefa): void {
    this.dialog.open(SimpleDialogComponent,
      {data: {texto: 'Confirma a remoção da tarefa? Esta ação não poderá ser desfeita!'}})
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.executaRemocao(tarefa);
        }
      });
  }

  executaRemocao(tarefa: Tarefa): void {
    this.tarefaService.delete(tarefa).subscribe(() => {
      const index = this.tarefas.indexOf(tarefa);
      this.tarefas.splice(index, 1);
    });
  }

  adicionaTarefa(): void {
    this.dialog
      .open(TarefaFormComponent, {width: 'auto'})
      .afterClosed()
      .subscribe(result => {
        if (result != null) {
          result.lista = this.lista.id;
          this.salvarTarefa(result);
        }
      });
  }

  resetaLista(): void {
    this.dialog.open(SimpleDialogComponent,
      {data: {texto: 'Deseja excluir todas as tarefas? Esta ação não poderá ser desfeita!'}})
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.executaResetLista();
        }
      });
  }

  executaResetLista(): void {
    this.tarefaService.reset(this.lista.id)
      .subscribe(() => {
        this.tarefas = [];
      });
  }

  deletaLista(): void {
    this.confirmaRemocaoLista();
  }

  confirmaRemocaoLista(): void {
    this.dialog.open(SimpleDialogComponent,
      {data: {texto: 'Confirma a remoção da lista? Esta ação não poderá ser desfeita!'}})
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.executaRemocaoLista();
        }
      });
  }

  executaRemocaoLista(): void {
    this.listaService.delete(this.lista.id)
      .subscribe(() => {
        this.router.navigate(['/']);
      });
  }
}
