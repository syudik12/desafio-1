import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ListaComponent} from './components/service/lista/lista.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {HeaderComponent} from './components/template/header/header.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {MatDialogModule} from '@angular/material/dialog';
import {MatRadioModule} from '@angular/material/radio';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {TarefaComponent} from './components/service/tarefa/tarefa.component';
import {ListaFormComponent} from './components/service/lista/lista-form/lista-form.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {TarefaFormComponent} from './components/service/tarefa/tarefa-form/tarefa-form.component';
import {MatSidenavModule} from "@angular/material/sidenav";
import { SimpleDialogComponent } from './components/template/simple-dialog/simple-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaComponent,
    HeaderComponent,
    TarefaComponent,
    ListaFormComponent,
    TarefaFormComponent,
    SimpleDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatToolbarModule,
    MatExpansionModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatDialogModule,
    MatRadioModule,
    MatCheckboxModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatSidenavModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
