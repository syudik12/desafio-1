import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListaComponent} from './components/service/lista/lista.component';
import {TarefaComponent} from './components/service/tarefa/tarefa.component';

const routes: Routes = [
  {
    path: '',
    component: ListaComponent
  },
  {
    path: 'listas/:id',
    component: TarefaComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
